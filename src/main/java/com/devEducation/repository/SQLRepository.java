package com.devEducation.repository;

import java.sql.*;

public class SQLRepository {

    private static final String URL = "jdbc:mysql://localhost:3306/randomdata?useUnicode=true&serverTimezone=UTC";
    private static final String USER = "root";
    private static final String PASSWORD = "Lokolol12345";
    private Statement statement;
    private ResultSet resultSet;
    private Connection connection;


    public SQLRepository() {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            connection = DriverManager.getConnection(URL, USER, PASSWORD);
            if (connection != null) {
                System.out.println("Connected to the database test1");
            }
        } catch (ClassNotFoundException e) {
            System.err.format("Driver error");
            e.printStackTrace();
        } catch (SQLException e) {
            System.err.format("Connection error");
        }
        createTable();
        clearTable();
    }

    public void insertValue(int number) throws SQLException {
        statement = connection.createStatement();
        String queryForInsertRandomData = "INSERT INTO randomdata.numbers (number) VALUES ("+number+");";
        statement.executeUpdate(queryForInsertRandomData);
    }

    public String getNumbers() throws SQLException {
        StringBuilder stringBuilder = new StringBuilder();
        statement = connection.createStatement();
        String queryForGetNumbers = "SELECT * FROM numbers";
        resultSet = statement.executeQuery(queryForGetNumbers);

        while (resultSet.next()){
            stringBuilder
                    .append(resultSet.getString(1))
                    .append(",");
        }
        if (stringBuilder.length() > 0) {
            stringBuilder.deleteCharAt(stringBuilder.length() - 1);
        }
        return stringBuilder.toString();
    }

    private void createTable() {
        try {
            statement = connection.createStatement();
            String createNumbersTableQuery = "CREATE TABLE IF NOT EXISTS numbers("
                    + "number   INT  NOT NULL);";
            statement.executeUpdate(createNumbersTableQuery);

            System.out.println("Tables successfully created");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void clearTable(){
        try {
            statement = connection.createStatement();
            String clearNumbersTableQuery = "TRUNCATE TABLE numbers";
            statement.executeUpdate(clearNumbersTableQuery);

            System.out.println("Tables successfully created");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
