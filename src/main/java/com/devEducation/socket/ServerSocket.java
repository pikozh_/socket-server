package com.devEducation.socket;

import com.devEducation.model.Generator;
import com.devEducation.repository.SQLRepository;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.sql.SQLException;

public class ServerSocket {

    private ServerSocketChannel serverSocketChannel;
    private ByteBuffer byteBuffer = ByteBuffer.allocate(1024);
    SocketChannel client = null;
    SQLRepository sqlRepository = null;

    public ServerSocket(int port){
        try {
            serverSocketChannel = ServerSocketChannel.open();
            serverSocketChannel.socket().bind(new InetSocketAddress(port));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public SocketChannel accept() throws IOException {
        return serverSocketChannel.accept();
    }

    public ByteBuffer getByteBuffer() {
        return byteBuffer;
    }

    public void workWithClient() throws IOException, SQLException, InterruptedException {
        while (true) {
            try {
                client = accept();
            } catch (IOException e) {
                e.printStackTrace();
            }
            ByteBuffer byteBuffer = getByteBuffer();
            String response = "";
            if (client.read(byteBuffer) > 0) {

                response = new String(byteBuffer.array(), "UTF-8");
                byteBuffer.clear();

//                ПОЧЕМУ false???
//                System.out.println(response.equals("Hello"));

                if (response.startsWith("H")) {
                    Generator generator = new Generator();
                    sqlRepository = new SQLRepository();
                    generator.generateAndWrite(byteBuffer, client, sqlRepository);
                }

                if (response.startsWith("G")) {
                    String listOfNumbersCSV = sqlRepository.getNumbers();
                    byteBuffer.put(listOfNumbersCSV.getBytes());
                    byteBuffer.flip();

                    while (byteBuffer.hasRemaining()) {
                        client.write(byteBuffer);
                        String check = new String(byteBuffer.array(), "UTF-8");
                        System.out.println(check);
                    }
                }
            }
        }
    }
}
