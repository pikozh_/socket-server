package com.devEducation;


import com.devEducation.socket.ServerSocket;

import java.io.IOException;

import java.sql.SQLException;

public class Main {
    private static final int PORT = 9000;

    public static void main(String[] args) throws IOException, InterruptedException, SQLException {

        ServerSocket serverSocket = new ServerSocket(PORT);
        serverSocket.workWithClient();
    }
}
