package com.devEducation.model;

import com.devEducation.repository.SQLRepository;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;
import java.sql.SQLException;
import java.util.Random;

public class Generator {
    private Random random;

    public void generateAndWrite(ByteBuffer byteBuffer, SocketChannel client, SQLRepository sqlRepository) throws SQLException, IOException, InterruptedException {
        random = new Random();

        for (int i = 0; i < 100; i++) {
            int a = random.nextInt(1000);
            System.out.println(a);
            sqlRepository.insertValue(a);
            String number = String.valueOf(a);
            byteBuffer.put(number.getBytes());
            byteBuffer.flip();

            while (byteBuffer.hasRemaining()) {
                client.write(byteBuffer);
            }
            Thread.sleep(1000);
            byteBuffer.clear();
        }

    }
}
